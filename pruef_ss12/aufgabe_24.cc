#include <iostream>

struct H {};
struct J : public H {};

void g(int i) {
   switch(i) {
      case 0:
         throw 0;
         break;
      case 1:
         throw J();
         break;
      default:
         throw H();
   }
}

void h() {
   for(int i=0; i<3; i++) {
      try {
         g(i);
      }
      catch(int e) {
         std::cout << "h(): catch(int)\n";
      }
      catch(J e) {
         std::cout << "h(): catch(J)\n";
      }
   }
}

int main() {
   try {
      h();
   }
   catch(H e) {
      std::cout << "main(): catch(H)\n";
   }
}

/* Ausgabe:
 * h(): catch(int)
 * h(): catch(J)
 * main(): catch(H)
 */
