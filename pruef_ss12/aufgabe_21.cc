#include <iostream>

using namespace std;

struct A
{
   void a() {}
   virtual void a() const = 0;
};

struct B : public A
{
   B()
   {
      n = ++N;
      log("B::B()");
   }
   virtual ~B() { log("B::~B()"); }
   void log(const string& s) const { cout << s << ": " << n << "\n"
      ; }
   void a() { log("B::a()"); }
   void a() const { log("B::a() const"); }
   int n;
   static int N;
};

int B::N;

struct C : public B
{
   C() { log("C::C()"); }
   ~C() { log("C::~C()"); }
   void a() { log("C::a()"); }
   void a() const { log("C::a() const"); }
};

int main()
{
   A* a[2] = { 
      new B(),           // B::B(): 1
      new C()            // B::B(): 2
                         // C::C(): 2
   };

   /********************/

   /*
    * Andere Schreibweise: 
    * for (int i = 0; i < 2; i++) {
    *    A* e = a[i];
    *    e->a();
    * }
    */
   for(A* e : a) 
   {
      e->a();            // keine Ausgabe, da void a() in class A nicht virtual
                         // ist und e vom type A* ist 
   }

   /********************/

   for(const A* e : a)
   {
      e->a();            // B::a() const: 1
                         // C::a() const: 2
   }

   /********************/

   B* b[2] = { 
      new B(),           // B::B(): 3
      new C()            // B::B(): 4
                         // C::C(): 4
   };

   /********************/

   for(B* e : b)
   {
      e->a();            // Beides mal B::a(), da void B::a(); nicht virtual ist
                         // B::a(): 3
                         // B::a(): 4
   }

   /********************/

   for(const B* e : b)
   {
      e->a();            // B::a() const: 3
                         // C::a() const: 4
   }

   /********************/

   for(A* e : a)
   {
      delete e;          // keine Ausgabe, da ~A() nicht virtual ist
   }

   /********************/

   for(B* e : b)
   {
      delete e;          // B::~B(): 3
                         // C::~C(): 4
                         // B::~B(): 4
   }

   /********************/

   C c;                  // B::B(): 5
                         // C::C(): 5
   c.a();                // C::a(): 5
   const C& r = c;       // keine Ausgabe, da keine neues Objekt
   r.a();                // C::a() const: 5
}
                         // beim Verlassen der main Funktion wird C c verstört
                         // C::~C(): 5
                         // B::~B(): 5
