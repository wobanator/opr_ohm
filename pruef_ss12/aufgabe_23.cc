#include <iostream>

using namespace std;

struct E
{
   E() {}
};

void f(const E& e) { cout << "f(const E&)\n"; }
void f(E& e) { cout << "f(E&)\n"; }

struct G
{
   G() {}
};

void f(G& e) { cout << "f(G&)\n"; }
void f(G&& e) { cout << "f(G&&)\n"; }
void f(const G& e) { cout << "f(const G&)\n"; }

int main()
{
   E e1;
   const E e2;
   G g1;
   const G g2;
   f(e1);                // f(E&)
   f(e2);                // f(const E&)
   f(move(e1));          // f(const E&)
   f(E());               // f(const E&)
                         // existiert kein Funktionsprototyp der einen R-Wert
                         // als Parameter hat, wird stattdessen die Funktion
                         // mit dem const E & Parameter aufgerufen

   /*******************/

   f(g1);                // f(G&)
   f(g2);                // f(const G&)
   f(move(g1));          // f(G&&)
   f(G());               // f(G&&)
}
