#include <iostream>

using namespace std;

struct D
{
   char c;
   D(char c) : c(c) {}
   D& operator+(const D& d)
   {
      std::cout << c << "+" << d.c << "\n";
      return *this;
   }
   D& operator=(const D& d)
   {
      std::cout << c << "=" << d.c << "\n";
      return *this;
   }
   D& operator=(const D&& d)
   {
      std::cout << c << ".operator=(" << d.c << ")\n";
      return *this;
   }
};


int main()
{
   D a('a'), b('b'), c('c');
   a = D('d');           // a.operator=(d)


   /********************/

   a + b + c;            // a+b
                         // a+c
                         // operator+ ist links assotiativ, das heißt es wird
                         // zunächst a + b aufgerufen und dann wird der
                         // Rückgabewert + c aufgerufen

   /********************/

   a = b = c;            // b=c
                         // a=b
                         // operator= ist rechts assotiativ, das heißt es wird
                         // zunächst b = c aufgerufen und dann wird der
                         // Rückgabewert a zugewiesen
}
