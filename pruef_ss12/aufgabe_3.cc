#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

class Musikinstrument 
{
   public:
      ~Musikinstrument() {}
      virtual string liefereName() const = 0;
      virtual void spielen() const = 0;
};

class MusikinstrumentImpl : public Musikinstrument 
{
   public:
      MusikinstrumentImpl(string name)
      {
         cout << "MusikinstrumentImpl(string): " << name << "\n";
         this->name = name;
      }
      ~MusikinstrumentImpl() 
      { cout << "~MusikinstrumentImpl(): " << name << "\n"; }

      string liefereName() const { return name; }
   private:
      string name;
};

class Trompete : public MusikinstrumentImpl
{
   public:
      Trompete() : MusikinstrumentImpl("Trompete") { cout << "Trompete()\n"; }
      ~Trompete() { cout << "~Trompete()\n"; }
      void spielen() const { cout << "TRÖÖÖÖT" << endl; }
};

class Klavier : public MusikinstrumentImpl 
{
   public:
      Klavier() : MusikinstrumentImpl("Klavier") { cout << "Klavier()\n"; }
      ~Klavier() { cout << "~Klavier()\n"; }
      void spielen() const { cout << "KLIMPER" << endl; }
};

class Musiker 
{
   public:
      Musiker(string name, const Musikinstrument* instrument)
      {
         cout << "Musiker(string, const Musikinstrument*): " << name << "\n";
         this->name = name;
         this->instrument = instrument;
      }
      virtual ~Musiker() {
         cout << "~Musiker(): " << name <<"\n";
         delete instrument;
      }
      void spielen() const {
         cout << name << " spielt " << instrument->liefereName() << ": ";
         instrument->spielen(); 
      }
      string liefereName() const { return name; }
   private:
      string name;
      const Musikinstrument *instrument;
};

template <const int ANZAHL_TROMPETER, const int ANZAHL_PIANISTEN>
class Orchester 
{
   public:
      Orchester();
      virtual ~Orchester() {
         cout << "~Orchester()\n";
         for(unsigned int i=0; i<musiker.size(); i++) {
            delete musiker[i];
         }
      }
      void operator*() const
      {
         for(unsigned int i=0; i<musiker.size(); i++) {
            musiker[i]->spielen();
         }
      }
   private:
      vector<Musiker*> musiker;
};

template <const int ANZAHL_TROMPETER, const int ANZAHL_PIANISTEN>
Orchester<ANZAHL_TROMPETER, ANZAHL_PIANISTEN>::Orchester() {
   cout << "Orchester()\n";
   for(int i=0; i<ANZAHL_TROMPETER; i++) {
      stringstream name;
      name << (i+1) << ".Trompeter";
      Musikinstrument * instrument = new Trompete();
      Musiker * trompeter = new Musiker(name.str(), instrument);
      this->musiker.push_back(trompeter);
   }
   for(int i=0; i<ANZAHL_PIANISTEN; i++) {
      stringstream name;
      name << (i+1) << ".Pianist";
      Musikinstrument * instrument = new Klavier();
      Musiker * pianist = new Musiker(name.str(), instrument);
      this->musiker.push_back(pianist);
   }
}
int main() {
   cout << "+++ Orchester besetzen\n";
   Orchester<2,1> orchester;
   cout << "+++ Das Konzert beginnt\n";
   *orchester;
   cout << "+++ Applaus\n";
}
