#include "gemuesehandler.h"
#include "gemuesefabrik.h"
#include "kunde.h"
#include <iostream>

using namespace std;

void GemueseHandler::abbonieren(Kunde* kunde)
{
   cout << "juhu neuer Kunde: " << kunde->liefereName() << endl;
   kunden.push_back(kunde);
}

void GemueseHandler::kuendigen(Kunde* kunde)
{
   cout << "schade Kunde hat keine Lust mehr: " << kunde->liefereName() << endl;
   // suche kunde auf der liste
   bool kundenGefunden = false;
   for (vector<Kunde*>::iterator it = kunden.begin(); it != kunden.end(); ++it) {
      if (*it == kunde) {
         // kunde auf meiner Liste gefunden
         kunden.erase(it);
         kundenGefunden = true;
         break;
      }
   }

   if (!kundenGefunden) {
      cout << "hey du hast ja noch gar nicht abboniert!!!" << endl;
   }
}

void GemueseHandler::liefern(const std::string &gemueseName)
{
   // fabrik kennt mein gemuese nicht
   if (GemueseFabrik::gibtEs(gemueseName) == false) {
      cout << "ich kann kein " <<  gemueseName << " liefern" << endl;
      return;
   }

   cout << "neue Lieferung: " << gemueseName << endl;

   for (unsigned i = 0; i < kunden.size(); i++) {
      // mit der fabrik wird aus dem string ein Gemuese erstellt
      // für jeden Kunden wird ein neues Gemüse erstellt
      Gemuese * gemuese = GemueseFabrik::erstelleGemuese(gemueseName);
      kunden[i]->beliefern(gemuese);
   }
}
