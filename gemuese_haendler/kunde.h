#pragma once
#include <string>

class Gemuese;

class Kunde
{
   private:
      std::string name;

   public:
      Kunde(const std::string & name) : name(name) {}

      std::string liefereName() const { return name; }

      void beliefern(Gemuese * gemuese);
};
