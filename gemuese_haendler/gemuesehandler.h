#pragma once

#include <vector>
#include <string>

class Kunde;

class GemueseHandler
{
   private:
      std::vector<Kunde*> kunden;

   public:
      void abbonieren(Kunde*);
      void kuendigen(Kunde*);
      void liefern(const std::string &gemueseName);
};
