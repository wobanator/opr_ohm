#include "gemuesehandler.h"
#include "kunde.h"

#include <iostream>

using namespace std;

int main()
{
   Kunde karl("Karl");
   Kunde fritz("Fritz");
   Kunde tobi("Tobi");
   Kunde anna("Anna");

   GemueseHandler handler;

   handler.liefern("Spinat");
   handler.liefern("Karotten");

   cout << endl << "der Händler macht extrem viel Werbung" << endl;
   handler.abbonieren(&karl);
   handler.abbonieren(&anna);

   handler.liefern("Spinat");
   handler.liefern("Wirsing");

   cout << endl << "Karl und Anna laden ein befreundetes Paar zum Essen ein" << endl;
   handler.abbonieren(&fritz);
   handler.abbonieren(&tobi);

   handler.liefern("Spinat");
   
   cout << endl << "Fritz wurde ziemlich schlecht von verdorbenen Spinat" << endl;
   handler.kuendigen(&fritz);

   handler.liefern("Wirsing");

}
