#pragma once

#include <string>

class Gemuese;

class GemueseFabrik
{
   public:
      static bool gibtEs(const std::string &name);

      // erstelle Gemuese aus string
      static Gemuese * erstelleGemuese(const std::string &name);
};


