#include "gemuesefabrik.h"
#include "spinat.h"
#include "wirsing.h"

bool GemueseFabrik::gibtEs(const std::string &name)
{
   if (name == "Spinat") return true;
   if (name == "Wirsing") return true;
   return false;
}

Gemuese * GemueseFabrik::erstelleGemuese(const std::string &name) 
{
   if (name == "Spinat") return new Spinat;
   if (name == "Wirsing") return new Wirsing;
   return NULL;
}
