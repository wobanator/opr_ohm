#pragma once

#include <string>

class Gemuese
{
   private:
      std::string name;

   public:
      virtual ~Gemuese() {}
      Gemuese(const std::string &name) : name(name) {}
      std::string liefereName() const { return name; }
};
