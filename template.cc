#include <string>
#include <iostream>

using namespace std;


template <typename T>
struct Container
{
	T * p;
};

struct Integer { int i; }; 
struct Double  { double d; };

void func(Container<Integer> a, Container<Double> b)
{

}

template <typename C1, typename C2>
bool operator==(Container<C1> a, Container<C2> b)  { return static_cast<void*>(a.p) == static_cast<void*>(b.p); }

/*
bool operator==(Container<Integer> a, Container<Double> b)  { return static_cast<void*>(a.p) == static_cast<void*>(b.p); }
bool operator==(Container<Integer> a, Container<string> b)  { return static_cast<void*>(a.p) == static_cast<void*>(b.p); }
bool operator==(Container<string> a,  Container<Integer> b) { return static_cast<void*>(a.p) == static_cast<void*>(b.p); }
 */

int main()
{
	Integer ganz_zahl;
	Double rational;
	string zeichenkette = "hallo";
	
	Container<Integer> int_cont = { &ganz_zahl };
	Container<Double> double_cont = { &rational };
	Container<string> str_cont = { &zeichenkette };
	
	func(int_cont, double_cont);
	
	if (int_cont == double_cont /* operator==(int_cont, double_cont) */ ) cout << "gleich" << endl;
	else cout << "nicht gleich" << endl;

	if (int_cont == str_cont) cout << "gleich" << endl;
	else cout << "nicht gleich" << endl;
	
	if (str_cont == int_cont) cout << "gleich" << endl;
	else cout << "nicht gleich" << endl;
	
	if (ganz_zahl == rational) cout << "gleich" << endl;
	else cout << "nicht gleich" << endl;
}