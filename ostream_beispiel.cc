
#include <iostream>
#include <string>
#include <sstream>

class Eintrag
{
   private:
      int index;
      std::string wert;

   public:
      Eintrag(const std::string &wert) : index(-1), wert(wert) {}
      Eintrag(int idx, const std::string & wert) : index(idx), wert(wert) {}
      int getIndex() const { return index; }
      std::string getWert() const { return wert; }

      std::string format() const
      {
         std::stringstream s;
         s << index << ":" <<  wert;
         return s.str();
      }

      void operator<<(int neuerIndex) { index = neuerIndex; }           // e << 10;
      void operator<<(const std::string &neueWert) { wert = neueWert; } // e << "wert";

      // erlaube der externen Funktion auf private elemente zuzugreifen
      friend std::ostream & operator<<(std::ostream &out, const Eintrag &e);
};


// um diesen Operator intern zu implementieren, müsse man in die Klasse ostream
// rein. das will man nicht
std::ostream & operator<<(std::ostream &out, const Eintrag &e) 
{
   out << e.index << ":" <<  e.wert; // das geht trotz private -> friend
   // out << e.getIndex() << ":" <<  e.getWert(); // ohne friend
   return out;
}

using namespace std;

int main()
{
   Eintrag e("ohne Index");
   Eintrag e8(8, "acht"), e9(9, "neun"), e10(10, "zehn");

   cout << e8.getIndex() << ":" << e8.getWert() << endl;

   cout << e9.format() << endl;

   cout << e10;  // operator<< (cout, e10);
   cout << endl; // operator<< (cout, endl);

   cout << e << endl; // operator<< ( operator<< ( cout, e ), endl );

   e << 100;
   e << "hundert";
   cout << e << endl;
}
