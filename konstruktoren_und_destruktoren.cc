#include <iostream>
#include <string>

using namespace std;

struct A
{
   string s;
   A(const string &s):s(s) { cout << __PRETTY_FUNCTION__ << ": " << s << endl; }
   virtual ~A()            { cout << __PRETTY_FUNCTION__ << ": " << s << endl; }
};

struct B : A
{
   B(const string &s):A(s) { cout << __PRETTY_FUNCTION__ << ": " << s << endl; }
   ~B()                    { cout << __PRETTY_FUNCTION__ << ": " << s << endl; }
};


int main()
{
   cout << "A a1(\"a1\");" << endl;
   A a1("a1");

   cout << "A * aptr = new A(\"aptr\");" << endl;
   A * aptr = new A("aptr");

   cout << "A & aref = *(new A(\"aref\"));" << endl;
   A & aref = *(new A("aref"));

   cout << "B b1(\"b1\");" << endl;
   B b1("b1");

   cout << "B * bptr = new B(\"bptr\");" << endl;
   B * bptr = new B("bptr");

   cout << "B & bref = *(new B(\"bref\"));" << endl;
   B & bref = *(new B("bref"));

   cout << "A * aptr_auf_b = new B(\"aptr_auf_b\");" << endl;
   A * aptr_auf_b = new B("aptr_auf_b");

   cout << "// hier wird der virtuelle destruktor wichtig" << endl;
   // hier wird der virtuelle destruktor wichtig

   cout << "delete aptr_auf_b; // hier wird A::~A();" << endl;
   delete aptr_auf_b; // hier wird A::~A();

   cout << "delete bptr;" << endl;
   delete bptr;

   cout << "delete &bref;" << endl;
   delete &bref;

   cout << "delete aptr;" << endl;
   delete aptr;

   cout << "delete &aref; " << endl;
   delete &aref; 
   
   cout << endl << "ende des programms" << endl;
}
