#!/bin/bash

function make_makefile_in
{
    dir=$1
    makefile=$1Makefile.in

    echo -n "create $makefile ... "

    [ -f $makefile ] && rm $makefile

    echo "# $makefile" >> $makefile
    echo ""            >> $makefile

    for directory in $(ls -d $dir*/ 2>&-)
    do
        echo "include \$(SRCPATH)${directory}Makefile.in" >> $makefile
    done

    echo "" >> $makefile
    echo "SOURCES += \\" >> $makefile

    for file in $(ls $dir*.cpp 2>&-)
    do
        echo "   \$(SRCPATH)$file \\" >> $makefile
    done
    echo "" >> $makefile

    echo -e "done\n"
}


for dir in $(find -type d | grep -v .svn)
do
    dirname="${dir:2}"
    if [ "$dirname" != "" ]
    then
        dirname=$dirname/
    fi
    make_makefile_in "$dirname"
done
