#include <iostream>
#include <string>

using namespace std;

struct A
{
   virtual void a() = 0;
};

class B : public A
{
   public:
      B()
      {
         ausgabe("B()");
      }
      virtual ~B()
      {
         ausgabe("~B()");
      }
      static void ausgabe(const string& text)
      {
         cout << "#" << z << ": "
            << text << endl;
         z++;
      }
   private:
      static int z;
};

int B::z = 0;

class C : public B
{
   public:
      C()
      {
         ausgabe("C()");
      }
      virtual ~C()
      {
         ausgabe("~C()");
      }
      void a()
      {
         ausgabe("C::a()");
      }
      void c()
      {
         ausgabe("C::c()");
      }
};

class D : public C
{
   public:
      D()
      {
         ausgabe("D()");
      }
      virtual ~D()
      {
         ausgabe("~D()");
      }
      void a()
      {
         ausgabe("D::a()");
      }
      void c()
      {
         ausgabe("D::c()");
      }
      void d()
      {
         ausgabe("D::d()");
         C::a();
      }
};

int main()
{
   B::ausgabe("START");   // #0: START
   A* za = new C();       // #1: B() 
                          // #2: C()
   za->a();               // #3: C::a()
   delete za;             // keine Ausgabe da ~A nicht virtual

   /*********************/

   B* zb = new C();       // #4: B()
                          // #5: C()
   zb->a();               // #6: C::a()
   delete zb;             // #7: ~C()
                          // #8: ~B()

   /*********************/

   D d;                   // #9: B()
                          // #10: C()
                          // #11: D()
   A& ra = d;             // keine Ausgabe da kein neues Objekt
   ra.a();                // #12: D::a()

   /*********************/

   C& rc = d;             // keine Ausgabe da kein neues Objekt
   rc.a();                // #13: D::a()
                          // da void a() in class C auch virtual ist
   rc.c();                // #14: C::c()
                          // da void c() in class C nicht virtual ist

   /*********************/

   D& rd = d;             // keine Ausgabe da kein neues Objekt
   rd.a();                // #15: D::a()
   rd.c();                // #16: D::c()
   rd.d();                // #17: D::d()
                          // #18: C::a()

   /*********************/

   B::ausgabe("ENDE");    // #19: ENDE
}
                          // mit Schließen der Klammer wird Objekt D d zerstört
                          // #20: ~D()
                          // #21: ~C()
                          // #22: ~B()
