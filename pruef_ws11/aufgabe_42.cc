#include <iostream>

using namespace std;

/* 1. Wandeln Sie die Klasse Matrix in eine Template-Klasse mit drei
 * Template-Parametern für den Datentyp der Matrixelemente und die x- und
 * y-Dimension der Matrix um. */
template <const int X_SIZE, const int Y_SIZE, typename T>
class Matrix
{
   private:
      T werte[X_SIZE][Y_SIZE];
   public:
      void setzeWert(int x, int y, T value)
      {
         werte[x][y] = value;
      }
      T holeWert(int x, int y) const
      {
         return werte[x][y];
      }
      void addiere(const Matrix& m)
      {
         for(int x=0; x<X_SIZE; x++)
         {
            for(int y=0; y<Y_SIZE; y++)
            {
               werte[x][y] += m.werte[x][y];
            }
         }
      }
      void ausgabe()
      {
         for(int y=0; y<Y_SIZE; y++)
         {
            for(int x=0; x<X_SIZE; x++)
            {
               cout << werte[x][y] << '\t';
            }
            cout << endl;
         }
      }
};

int main(void)
{
   /* 2. Ändern Sie die main()-Funktion ab, indem Sie die Template-Klasse für
    * den Datentyp double und die x-Dimension 3 und y-Dimension 2 verwenden. */
   Matrix<3, 2, double> m1, m2;

   for(int x=0; x<3; x++)
   {
      for (int y=0; y<2; y++)
      {
         /* 3. Initialisieren Sie m1 und m2 so, dass das Programm diese Ausgabe
          * liefert: */
         m1.setzeWert(x,y,(x+1)*0.1 + (y+1)*0.01); 
         m2.setzeWert(x,y,(x+1)     + (y+1)*0.1);
      }
   }

   cout << "m1:\n";
   m1.ausgabe();
   cout << "\nm2:\n";
   m2.ausgabe();

   m1.addiere(m2);
   cout << "\nm1:\n";
   m1.ausgabe();
}
