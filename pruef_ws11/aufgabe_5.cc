#include <iostream>
#include <string>

using namespace std;

/* 2. Entwerfen Sie hierzu eine eigene Exception-Klasse mit einer öffentlichen
 * Elementfunktion string beschreibung() const, die den Grund der Ausnahme
 * beschreibt. */
class Ausnahme
{
   public:
      Ausnahme(const string &beschreibung) : _beschreibung(beschreibung)  {}
      string beschreibung() const { return _beschreibung; }

   private:
      string _beschreibung;
};

class Matrix
{
   private:
      int werte[2][2];
   public:
      /* 1. Überprüfen Sie in der Funktion void setzeWert(int x, int y, int
       * value) die Funktionsargumente x und y auf Gültigkeit und werfen Sie
       * eine Ausnahme, falls die Argumente außerhalb des definierten
       * Wertebereichs liegen. */
      void setzeWert(int x, int y, int value)
      {
         if (x < 0 || x >= 2 || y < 0 || y >= 2) {
            throw Ausnahme("Ungültiger Index");
         }
         werte[x][y] = value;
      }
      void addiere(const Matrix& m)
      {
         for(int x=0; x<2; x++)
         {
            for(int y=0; y<2; y++)
            {
               werte[x][y] += m.werte[x][y];
            }
         }
      }
      void ausgabe()
      {
         for(int y=0; y<2; y++)
         {
            for(int x=0; x<2; x++)
            {
               cout << werte[x][y] << '\t';
            }
            cout << endl;
         }
      }
};

int main(void)
{
   /* 3. Überprüfen Sie in der main()-Funktion den kritischen Funktionsaufruf
    * und leiten Sie im Ausnahmefall die Beschreibung der Ausnahme auf die
    * Konsole. (Hinweis: Sie müssen hier nur das Programm modifizieren und
    * nicht den Inhalt des Konsolenfenster darstellen.) */
   try {
      Matrix m;

      for(int x=0; x<2; x++)
      {
         for (int y=0; y<2; y++)
         {
            m.setzeWert(x,y,10*(x+1)+y+1);
         }
      }

      cout << "m:\n";
      m.ausgabe();
   } catch (const Ausnahme &ausnahme)  {
      cerr << "Programmfehler: " << ausnahme.beschreibung() << endl;
      return 1;
   }
}
