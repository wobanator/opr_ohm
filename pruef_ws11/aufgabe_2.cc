#include <iostream>
using namespace std;

class P
{
   public:
      int x;
      int y;
      P() { x = 0; y = 0; }
      P(int x, int y) 
      {
         this->x = x;
         this->y = y;
      }
};

ostream & operator<<(ostream &os, const P &p)
{
   os << "(" << p.x << "," << p.y << ")";
   return os;
}

class A
{
   public:
      virtual ~A() {}
      virtual void a() const = 0;
};

class B : public A
{
   public:
      B(const P &p) : p(p) {} // { this->p = p; }

   protected:
      void b() const
      { cout  << "B::p=" << p << endl; }

   private:
      P p;
};

class C : public B
{
   public:
      C(const P &p) : B(p)  {}
      void a() const { b(); }
};

int main()
{
   P p(10, 20);
   cout << "p=" << p << endl;
   C c(p);
   cout << "c.a() liefert: ";
   c.a();
}
