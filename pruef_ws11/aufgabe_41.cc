#include <iostream>

using namespace std;

class Matrix
{
   private:
      int werte[2][2];
   public:
      void setzeWert(int x, int y, int value)
      {
         werte[x][y] = value;
      }
      int holeWert(int x, int y) const
      {
         return werte[x][y];
      }

      /* void addiere(const Matrix& m) */
      Matrix & operator+=(const Matrix& m)
      {
         for(int x=0; x<2; x++)
         {
            for(int y=0; y<2; y++)
            {
               werte[x][y] += m.werte[x][y];
            }
         }
         return *this;
      }
};

/* void ausgabe() */
ostream & operator<<(ostream &os, const Matrix &mat)
{
   for(int y=0; y<2; y++)
   {
      for(int x=0; x<2; x++)
      {
         /* cout << werte[x][y] << '\t'; */
         os << mat.holeWert(x, y) << '\t';
      }
      /* cout << endl; */
      os << endl;
   }
   return os;
}

/* Ergänzter Subtraktionsoperator */
Matrix operator-(const Matrix &a, const Matrix &b)
{
   Matrix ret;

   for(int x=0; x<2; x++)
   {
      for (int y=0; y<2; y++)
      {
         int wert = a.holeWert(x,y) - b.holeWert(x,y);
         ret.setzeWert(x,y,wert);
      }
   }
   return ret;
}


int main(void)
{
   Matrix m1, m2, m3;

   for(int x=0; x<2; x++)
   {
      for (int y=0; y<2; y++)
      {
         m1.setzeWert(x,y,10*(x+1)+y+1);
         m2.setzeWert(x,y,100*(x+1)+10*(y+1));
      }
   }

   cout << "m1:\n" << m1 << endl;
   cout << "m2:\n" << m2 << endl;

   m1 += m2;
   cout << "m1:\n" << m1 << endl;

   m3 = m1 - m2;
   cout << "m3:\n" << m3 << endl;
}
