

   TABELLE t1;
   TABELLE t2;

   // vertausche die inhalte der beiden tabelle

   TABELLE tausch = t1; // kopiere t1 in tausch
   t1 = t2;             // kopiere t2 nach t1
   t2 = tausch;         // kopiere erneut tausch nach t2

   // blöööööööd !!!!
   // ich will eigentlich nur die Tabellen austauschen
   // ohne die Werte so oft kopieren zu müssen


   TABELLE tausch(move(t1));  // schreibe werte nach tausch 
                              // die werte in t1 werden nach dieser Zeile
                              // verwendet
                              //
                              // mit move sagt man, dass man t1 nicht mehr
                              // braucht nach dieser Zeile (temporärer Wert)
                              //
   t1 = move(t2);             // schreibe werte nach t1, damit wird t1 wieder
                              // ein verwendbares Objekt
                              //
                              // t2 wird nach der Zeile nicht mehr verwendet
                              // => man kann die Daten von t2 entfernen
                              //
                              // mit move sagt man, dass man t2 nicht mehr
                              // braucht nach dieser Zeile (temporärer Wert)
                              //
   t2 = move(tausch);         

   // durch die move aufrufe wird dafür gesorgt, dass die Verschiebe Semantik
   // Funktionen, also TABELLE(TABELLE &&) und TABELLE::operator=(TABELLE &&)
   // aufgerufen werden
   // in diesen muss nun die tabelle nicht kopiert sondern nur der Zeiger
   // umgesetzt werden, zusätzlich muss dafür gesorgt werden, dass in den
   // R-Wert-Objekten der Tabellenzeiger ungültig gesetzt wird. (um dopple
   // deletes zu verhindern)
