#include <iostream>
#include <cassert>

using namespace std;


/*---------------------Forwarddeklartion--------------------------*/
/*----------------------------------------------------------------*/

template<typename T, const int MAX_ANZAHL>
struct Vektor;

template<typename T, const int MAX_ANZAHL>
std::ostream& operator<< (std::ostream& os, Vektor<T,MAX_ANZAHL>& v);



/*---------------------struct-Vektor------------------------------*/
/*----------------------------------------------------------------*/

template <typename T, const int MAX_ANZAHL>
struct Vektor
{
private:
   T data[MAX_ANZAHL];

public:
    Vektor();
    T& operator[] (unsigned index);
};



/*---------------------Vektor-functions--------------------------*/
/*----------------------------------------------------------------*/

template <typename T, const int MAX_ANZAHL>
Vektor<T,MAX_ANZAHL>::Vektor()
{
   for (auto &i: data) i = 0;
}

template <typename T, const int MAX_ANZAHL>
T& Vektor<T,MAX_ANZAHL>::operator[](unsigned int index)
{
   assert(index >= 0 && index < MAX_ANZAHL);
   return data[index];
}

template <typename T, const int MAX_ANZAHL>
std::ostream& operator<<(std::ostream& os, Vektor<T,MAX_ANZAHL>& v)
{
   os << "(";
   for(unsigned int i=0; i < MAX_ANZAHL; i++)
      os <<" "<< v[i];
   os << " )";
   return os;
}

/*---------------------main---------------------------------------*/
/*----------------------------------------------------------------*/

int main()
{
    Vektor<double,5> v;
    cout<<v<<endl;
    v[0]=2.3;
    v[1]=1.9;
    cout<<v<<endl;
}
