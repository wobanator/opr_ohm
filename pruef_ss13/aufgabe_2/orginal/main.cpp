#include "B.h"
#include <iostream>
using namespace std;

void nachricht( const string& text) const
{
    cout << text << endl;
}

int main()
{
    nachricht("Anfang");

    B * zb = new B();
    zb->setzeWert(0);
    A * za = zb;
    B * zb2 = za;

    B& rb = * zb;
    rb[0] = 0;
    rb[1] = 1;
    rb[3] = 3;
    cout << "rb[0]=" << rb[0] << endl;

    const A& ra = rb;
    cout << "ra[0]=" << ra[0] << endl;

    delete za;
    nachricht("Ende");
};
