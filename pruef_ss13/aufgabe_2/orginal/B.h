#pragma once
#include "A.h"

struct B : public A
{
private:
    int wert;
public:
    B() : A(3) {}
    inline void setzeWert( int w);
    virtual string liefereKlassenname() const { return "B"; }
};
