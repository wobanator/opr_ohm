#pragma once
#include "Ausnahme.h"
#include <string>

using namespace std;

class A
{
private:
    int * feld;
    unsigned int laenge;

public:

    A( unsigned int laenge) : laenge(laenge)
    {
        feld = new int[laenge];
    }

    ~A();

    int & operator[]( unsigned int index)
    {
        if (index>=laenge)
        {
            throw Ausnahme();
        }
        return feld[index];
    }

    static virtual string liefereKlassenname() { return "A"; }
};

A::~A()
{
    if (feld!=nullptr)
    {
        delete[] feld;
    }
}
