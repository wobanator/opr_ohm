#include "B.h"
#include <iostream>
using namespace std;
/* eine Funktion außerhalb einer Klasse kann nicht const sein (kein this) */
void nachricht( const string& text) /* const */
{
    cout << text << endl;
}

int main()
{
    nachricht("Anfang");

    B * zb = new B();
    zb->setzeWert(0);
    A * za = zb;
    // B * zb2 = za; // Zuweisung Eltern-Zeiger an Kind-Zeiger nicht erlaubt

    B& rb = * zb;
    rb[0] = 0;
    rb[1] = 1;
    // rb[3] = 3; // Laufzeitfehler: Ueberschreitung des Indexes und kein try catch
    cout << "rb[0]=" << rb[0] << endl;
    /* auf das const Objekt ra kann die nicht const Funktion A::operator[] nicht aufgerufen werden */
    // const A& ra = rb;
    // cout << "ra[0]=" << ra[0] << endl;

    delete za;
    nachricht("Ende");
};
