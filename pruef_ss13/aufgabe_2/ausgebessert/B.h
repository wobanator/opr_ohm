#pragma once
#include "A.h"

struct B : public A
{
private:
    int wert;
public:
    B() : A(3) {}
    /* inline */ void setzeWert( int w); // inline führt zu undefined reference, da Funktion nicht in Header implementiert wird
    virtual string liefereKlassenname() const { return "B"; }
};
