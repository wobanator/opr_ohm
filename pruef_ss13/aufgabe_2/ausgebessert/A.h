#pragma once
#include "Ausnahme.h"
#include <string>

using namespace std;

class A
{
private:
    int * feld;
    unsigned int laenge;

public:

    A( unsigned int laenge) : laenge(laenge)
    {
        feld = new int[laenge];
    }
    /* ein virtueller Destruktor, könnte zu Laufzeitfehlern führen, da unter Umständen der Kindkonstruktor nicht aufgerufen wird */
    virtual ~A();

    int & operator[]( unsigned int index)
    {
        if (index>=laenge)
        {
            throw Ausnahme();
        }
        return feld[index];
    }
    /* eine Funktion kann nicht static und virtual sein. const hinzugefügt damit der code in Klasse B diese Funktion überschreibt */
    /* static */ virtual string liefereKlassenname() const { return "A"; }
};
/* multiple define, da Funktion in Header implementiert wird => inline */
inline A::~A()
{
    if (feld!=nullptr)
    {
        delete[] feld;
    }
}
