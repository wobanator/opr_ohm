


class A {
private:

   int intWert;

public:

   A() {}

   A(int i) { intWert = i; }

   A(const A& a) { intWert = a.intWert; }

   void setzeInt(int i) { intWert = i; }

   int liefereInt() const { return intWert; }

   const int& liefereInt2() const { return intWert; }

   void liefereInt3(int* i) const { *i = intWert; }

   void multipliziere(const int* a, int laenge) {
      for (int i = 0; i<laenge; i++) { intWert *= *(a++); }
   }
   
   A operator+(const A& a) const { return A(intWert + a.intWert); }
 };

int liefereBetrag(const A& a) { return a.liefereInt() *  a.liefereInt(); }

int main() {
   A a;
   a.setzeInt(2);
   const A c(3);
   const int& i = c.liefereInt2();
   int j;
   c.liefereInt3(&j);
   A b(a);
   b = a + c;
   int intFeld[3] = {2,3,4};
   a.multipliziere(intFeld,3);
}

/*
 *  + Zeile 15: zweckmäßig, da das Objekt, von dem ich das neue Objekt kopieren
 *  möchte, nicht durch das Kopieren nicht verändert werden soll
 *
 *  + Zeile 19: zweckmäßig, da ich nur lesend auf das Objekt zugreifen möchte.
 *  Durch const in Zeile 32 auch notwendig
 *
 *  + Zeile 21 liefereInt2() -> const <-: notwendig, da in Zeile 38 diese
 *  Funktion auf ein konstantes Objekt aufgerufen wird. (c.liefereInt2())
 *
 *  + Zeile 21 -> const <- int& liefereInt2(): notwendig, da bei konstanten
 *  Memberfunktionen die Membervariablen auch konstant sind.
 *
 *  + Zeile 23: notwendig, da in Zeile 40 diese Funktion auf ein konstantes
 *  Objekt aufgerufen wird. (c.liefereInt3(&j))
 *
 *  + Zeile 25: zweckmäßig, da man auf int-Feld, das als Parameter mitgegeben
 *  wird, nur lesend zugreifen möchte.
 *
 *  + Zeile 29: operator+(-> const <- A& a): zweckmäßig, da der linke Summand
 *  einer Summe nicht verändert werden soll.
 *
 *  + Zeile 29: operator+(A& a) -> const <-: zweckmäßig, da der rechte Summand
 *  einer Summe nicht verändert werden soll.
 *
 *  + Zeile 32: zweckmäßig, da die Betragsbildung das Objekt nicht verändern
 *  sollte.
 *
 *  Bemerkung:
 *  Man könnte teorisch natürlich auch bei allen int-Parametern ein const dazu
 *  setzten. Dies würde jedoch eher das Programm unleserlicher machen, als
 *  einen wirklichen Nutzen zu bewirken, da diese int-Parameter jeweils als
 *  Kopien übergeben werden.
 */
